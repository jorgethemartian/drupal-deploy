/**
 * Check end of string for a specified character.
 * @param {string} suffix - Character to check for.
 * @return {boolean} Return true or false
 */
if (typeof String.prototype.endsWith !== 'function') {
  String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
  };
}