/**
 * Required libraries
 */
var log = console.log;
var stdio = require('stdio');
var drush = require('./libraries/Drush');
var databases = require('./libraries/Databases');

// add strings helper file
require('./helpers/Strings');

/**
 * The application will execute drush commands to deploy database updates,
 * new modules installs, user account creation, variable migration,
 * and sequel execution for Drupal.
 */

/**
 * Get command line arguments
 * @param {string} environment - The environment we want to deploy to.
 * @param {string} path - The path of the Drupal website to run script for.
 */
var options = stdio.getopt({
  'environment': {key: 'e', description: 'The environment we want to deploy to.', mandatory: true, args: 1},
  'path': {key: 'p', description: 'The path of the Drupal website to run script for.', mandatory: true, args: 1},
  'newDb': {key: 'n', description: 'Add new database.', mandatory: false},
});

// check to see if string ends with a forward slash
if (!options.path.endsWith('/')) {
  // if not, let's add it
  options.path = options.path + '/';
}

var finished = false;
var filesExecuted = false;

// check to see if we are adding a new database
if (typeof options.newDb !== 'undefined') {
  var sqlExectued = databases.addDb(options);

  // after executing database import, execute drush deployment
  if (sqlExectued === true) {
    executeFiles();
  }
} else {
  executeFiles();
}

function executeFiles() {
  if (typeof options.args === 'object') {
    var fileList = options.args;

    log('Beginning Drush deployment process.');

    fileList.forEach(function(file) {
      drush.readFile(options, file);
    });
  }
}
