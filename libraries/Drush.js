/**
 * Required libraries
 */
var fs = require('fs');
var log = console.log;
var glob = require('glob');
var ini = require('ini');
var shell = require('shelljs');

// set the path to look in
var fileDeployPath = 'sites/*/deploy';

/**
 * Get modules ini file so we can run module updates.
 * @param {object} options - The command line arguments.
 * @param {string} file - The file we want to grab.
 * @return {file} - Return ini files.
 */
function modules(options, file) {
 /**
  * This is the process flag, it is used to let the
  * script know when it is safe to go on to the next
  * module step.
  * (Example: after finishing with enabling modules, set to
  * true and let script know to start disabling modules.)
  */
  var processFinished = false;

  /**
    * Processes the module file stanzas to disdable, uninstall or enable a module
    */
  function processModules(modules) {
    processFinished = false;
    var actions = ['disable', 'uninstall', 'enable'];
    var actionDef = {
      enable: {
        messageStart: 'Enabling',
        drushCommand: 'en'
      },
      disable: {
        messageStart: 'Disabling',
        drushCommand: 'dis'
      },
      uninstall: {
        messageStart: 'Uninstalling',
        drushCommand: 'pm-uninstall'
      }
    }
    // start disabling modules process
    actions.forEach(function(action) {
      if(!modules[action]) return;
      var moduleSet = modules[action].modules;
      if (typeof moduleSet  !== 'undefined') {
        log(actionDef[action].messageStart + ' modules ...' + "\n");

        // get last module to execute
        var lastModule = moduleSet[moduleSet.length - 1];

        // loop through modules and enable
        moduleSet.forEach(function(module) {
          // enable module
          shell.exec('drush ' + actionDef[action].drushCommand + ' ' + module + ' -y');
          var modPath = options.path + 'sites/all/modules/' + module;
          var modPath2 = options.path + 'sites/all/modules/custom/' + module;

          //for uninstall actions, also delete the module folders
          if (action === 'uninstall' && shell.test('-e', modPath))
            shell.rm('-rf', modPath);
          if (action === 'uninstall' && shell.test('-e', modPath2))
            shell.rm('-rf', modPath2);
          log("\n");

          // if we have executed the last module
          // set processFinished to true
          if (lastModule == module) {
            processFinished = true;
          }
        });
      } else {
        log('There are no modules to ' + action + '.' + "\n");

        // set processFinished to true
        processFinished = true;
      }
    });

  }


  // concat the file path with the site path
  filePath = options.path + fileDeployPath + '/*.' + file;

  // search for wildcard path and file
  glob(filePath, function (error, files) {
    // if file does not exist, throw error
    if (error || files == 0) {
      return log('File \'%s\' does not exist.' + "\n", file);
    }

    files.forEach(function(file, index) {

      fs.open(file, 'r', function(error, fd) {
        // check to see if error is not null
        // this error check is probably redundant
        if (error) {
          // if file does not exist, throw error
          // we are already checking above but just in case ...
          if (error.code == 'ENOENT') {
            return log('File \'%s\' does not exist.' + "\n", file);
          }
        }

        // read file contents
        var modules = ini.parse(fs.readFileSync(file, 'utf8'));

        // check to make sure environment is equal to the one requested
        if (options.environment === modules.environment) {
          // check to make sure drush exists.
          if (!shell.which('drush')) {
            return log('Script requires that drush be installed.' + "\n");
          }

          // move to directory the drupal site is in
          shell.cd(options.path);
          log('Switched to directory %s' + "\n", options.path);

          // update Drupal database
          var drushUpdateDBStatus = shell.exec('drush updatedb-status', { silent:true });

          // if there are no updates available
          if (drushUpdateDBStatus.output.indexOf('No database updates required') > -1) {
            // return message
            log(drushUpdateDBStatus.output);
          } else {
            log('There are database updates that need to be performed.' + "\n");
            shell.exec('drush updatedb -y');
            log("\n");
          }

          processModules(modules);

          if (processFinished === true && files.length -1 == index) {
            // set processFinished to false
            processFinished = false;

            // revert all features
            shell.exec('drush fra -y');

            // finally clear all drupal cache
            shell.exec('drush cc all');
          } else {
            log('An error occured while processing modules.' + "\n");
          }
        }
      });
    });
  });
}

/**
 * Get ini files so we can run updates.
 * @param {object} options - The command line arguments.
 * @param {string} file - The file we want to grab.
 * @return {file} - Return ini files.
 */
function users(options, file) {
  // concat the file path with the site path
  filePath = options.path + fileDeployPath + '/*.' + file;

 /**
  * This is the process flag, it is used to let the
  * script know when it is safe to go on to the next
  * user step.
  * (Example: after finishing with creating users, set to
  * true and let script know to start removing users.)
  */
  var processFinished = false;

  // search for wildcard path and file
  glob(filePath, function (error, files) {
    // if file does not exist, throw error
    if (error || files == 0) {
      return log('File \'%s\' does not exist.' + "\n", file);
    }

    files.forEach(function(file, index) {
      fs.open(file, 'r', function(error, fd) {
        // check to see if error is not null
        if (error) {
          // if file does not exist, throw error
          // we are already checking above but just in case ...
          if (error.code == 'ENOENT') {
            return log('File \'%s\' does not exist.' + "\n", file);
          }
        }

        // read file contents
        var users = ini.parse(fs.readFileSync(file, 'utf8'));

        // check to make sure environment is equal to the one requested
        if (options.environment === users.environment) {
          // check to make sure drush exists.
          if (!shell.which('drush')) {
            return log('Script requires that drush be installed.' + "\n");
          }

          // check to see that create accounts exist
          if (typeof users.create.account !== 'undefined') {
            // move to directory the drupal site is in
            shell.cd(options.path);
            log('Switched to directory %s' + "\n", options.path);

            // get last account that should be created
            var lastAccount = users.create.account[users.create.account.length - 1];

            // create new user accounts
            users.create.account.forEach(function(user) {
              // split the account string by commas and clear spaces
              user = user.split(/\s*,\s*/);
              // check to see if user exists
              var userAccountCheck = shell.exec('drush user-information ' + user[0], { silent:true });

              // if user does not exist
              if (userAccountCheck.output.indexOf('Could not find a uid for the search term') > -1) {
                // if not create user
                var createAccount = shell.exec('drush user-create ' + user[0] + ' --mail="' + user[1] + '" --password="' + user[2] + '"', { silent:true });

                if (createAccount.output.indexOf('There is already a user account with the name') < 0) {
                  log('User account ' + user[0] + ' has been created.' + "\n");
                } else {
                  log(createAccount.output);
                }

                // check to see if roles to add user to, have been specified
                if (typeof user[3] !== 'undefined') {
                  // add roles to array
                  var roles = user[3].split(/ +(?=[\w]+\:)/g);

                  // string variable to save roles names
                  var rolesString = '';

                  // add user to roles
                  roles.forEach(function(role) {
                    shell.exec('drush user-add-role '+ role +' --name=' + user[0]);
                    log("\n");

                    // save role name
                    rolesString += role;
                    if (roles.length > 1) {
                      // if there are more than one role, add a space to help string be readable
                      rolesString += ' ';
                    }
                  });

                  log('User account ' + user[0] + ' has been added to role(s) ' + rolesString + '.' + "\n");
                }
              } else {
                // return message
                log('The username ' + user[0] +' already exists.' + "\n");
                log(userAccountCheck.output + "\n");
              }
            });
            processFinished = true;
          }

          // check to see that remove accounts exist
          if (typeof users.remove.account !== 'undefined') {
            if (processFinished === true) {
              // remove user accounts
              users.remove.account.forEach(function(user) {
                // check to see if user exists
                var userAccountCheck = shell.exec('drush user-information ' + user, { silent:true });

                if (userAccountCheck.output.indexOf('Could not find a uid for the search term') < 0) {
                  log('Deleting user account \'' + user + '\'.' + "\n");
                  shell.exec('drush user-cancel ' + user + ' -y');
                } else {
                  // return message
                  log('The username ' + user +' does not exist.' + "\n");
                }
              });
            }
          }
        }
      });
    });
  });
}

/**
 * Process lists of files to be deleted
 * @param {object} options - The command line arguments.
 * @param {string} file - The file we want to grab.
 * @return {file} - Return ini files.
 */
function files(options, file) {
  // concat the file path with the site path
  filePath = options.path + fileDeployPath + '/*.' + file;

  // search for wildcard path and file
  glob(filePath, function (error, files) {
    // if file does not exist, throw error
    if (error || files == 0) {
      return log('File \'%s\' does not exist.' + "\n", file);
    }

    files.forEach(function(curFile, index) {
      var fileOpts = ini.parse(fs.readFileSync(curFile, 'utf8'));
      fileSet = fileOpts.remove.files;

      fileSet.forEach(function(f) {
        console.log('Deleting file ' + options.path + f +'\n\r');
        shell.rm('-rf', options.path + f);
      });
    });
  });

}

function readFile(options, file) {
  if (file === 'modules') {
    // execute modules file
    modules(options, file);

    return true;
  }

  if (file === 'users') {
    // execute users file
    users(options, file);

    return true;
  }

  if (file === 'files') {
    //execute files files
    files(options, file);

    return true
  }

  return false;
}

module.exports.readFile = readFile;
