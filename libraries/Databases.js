/**
 * Required libraries
 */
var fs = require('fs');
var log = console.log;
var glob = require('glob');
var ini = require('ini');
var shell = require('shelljs');

// set the path to look in
var fileDeployPath = 'sites/*/deploy';

/**
 * Get the sql dumps in the deploy directory and execute them
 */
function add(options) {
  // concat the file path with the site path
  filePath = options.path + fileDeployPath + '/*.sql';

  // search for wildcard path and file
  return glob(filePath, function (error, files) {
    // if file does not exist, throw error
    if (error || files.length == 0) {
      return log('There are no database SQL dumps available.' + "\n");
    }

    files.forEach(function(file, index) {
      fs.readFile(file, 'utf-8', function(error, data) {
        // check to make sure drush exists.
        if (!shell.which('drush')) {
          return log('Script requires that drush be installed.' + "\n");
        }

        // move to directory the drupal site is in
        shell.cd(options.path);
        log('Switched to directory %s', options.path);
        
        //Split data into individual statements and execute them
        var statements = data.split(';\n');
        statements.forEach(function(sql) {
          sql = sql.trim().replace(/`/g, '\\\`');
          if (sql.length === 0) 
            return;
          var results = shell.exec("drush sql-query \"" + sql + ";\"", { silent: true });

          // if user does not exist
          if (results.output.indexOf('ERROR') > -1) {
            log('Database import failed.' + "\n");
            log(results.output + "\n");
          } else {
            log('Successfully executed sql file %s.' + "\n", file);
            log(results.output);
          }
        });
      });
    });
  });
}

function addDb(options) {
  // return true if file returns object
  return typeof add(options) !== 'undefined';
}

module.exports.addDb = addDb;
