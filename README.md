# Deploy Script

The Deploy Script project was created to help in assisting with Drupal database settings updates deployments. The script helps with adding, enabling, disabling and uninstalling modules as well as help with reverting Feature modules. It helps with creating users and adding them to roles as well as delete existing users. It also helps with creating new external databases, and modifying existing databases with *.sql files.

## Installation

TODO: Describe the installation process

## Usage

### Modules

TODO: Write modules

### Users

TODO: Write users

### Roles

TODO: Write roles

### Files

TODO: Write files

### SQL Updates

TODO: Write SQL updates

### New Databases

TODO: Write new databases
